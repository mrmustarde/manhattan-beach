<?php

define('TEMPLATE_PATH', get_stylesheet_directory_uri());
define('ADMIN_URL', get_admin_url());
define('HOME_URL', get_home_url());

// css
require_once( 'include/styles.php' );

// scripts
require_once( 'include/scripts.php' );

// sidebars
require_once( 'include/sidebars.php' );

// menus
require_once( 'include/menus.php' );

// meta boxes for admin
require_once( 'include/metaboxes.php' );

// shortcodes
require_once( 'include/shortcodes.php' );

// specialized search
require_once ("include/searchFunction.php");


// removes wordpress menu in admin
function annointed_admin_bar_remove() {
        global $wp_admin_bar;

        $wp_admin_bar->remove_menu('wp-logo');
}

add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);


// replace logo at login
function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_stylesheet_directory_uri().'/images/custom-login-logo.gif) !important; }
    </style>';
}

add_action('login_head', 'my_custom_login_logo');



add_filter( 'gettext', 'change_post_to_article' );
add_filter( 'ngettext', 'change_post_to_article' );

function change_post_to_article( $translated ) 
{  
    $translated = str_replace( 'Post', 'Article', $translated );
    $translated = str_replace( 'post', 'article', $translated );
    $translated = str_replace( 'Media', 'Photo', $translated );
    $translated = str_replace( 'media', 'photo', $translated );
    return $translated;
}

?>