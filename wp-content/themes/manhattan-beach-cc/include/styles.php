<?php

/**
 * Register with hook 'wp_enqueue_scripts', which can be used for front end CSS and JavaScript
 */
add_action( 'wp_enqueue_scripts', 'prefix_add_my_stylesheet' );

/**
 * Enqueue plugin style-file
 */
function prefix_add_my_stylesheet() {
    // Respects SSL, Style.css is relative to the current file
    wp_register_style( 'prefix-style', 'http://dev-business.manhattanbeachchamber.com/integration/customerdefinedcss' );
    wp_enqueue_style( 'prefix-style' );

    wp_register_style( 'chamberMaster-style', TEMPLATE_PATH . '/chamberMaster.css' );
    wp_enqueue_style( 'chamberMaster-style' );
}
?>