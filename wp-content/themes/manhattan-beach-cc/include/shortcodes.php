<?php

// [bartag foo="foo-value"]
function chamber_events_accordion_func( $atts ) {


	extract( shortcode_atts( array( 'limit' => 5, 'category' => 'chamber-events', 'title'=> 'Events' ), $atts ) );

	$q = new WP_Query( array(
		'posts_per_page' =>  $limit,
		'category_name' => $category
	));

	$list = '<h3 class="accordion-header-title title-color gdl-title">' . $title . '</h3>
		<ul class="gdl-accordion">';

	while ( $q->have_posts() ) {
		$q->the_post();
		$list .= '<li class="gdl-divider"><h2 class="accordion-head title-color gdl-title">
				<span class="accordion-head-image active"></span>' . get_the_title() . '</h2>
			<div class="accordion-content" style="">' . get_the_excerpt( ) . ' <a class="event-read-more" href="' .get_permalink() . '" >Read More</a></div></li>';
	}

	wp_reset_query();

	return $list . '</ul>';

}
add_shortcode( 'chamber_events_accordion', 'chamber_events_accordion_func' );



// [bartag foo="foo-value"]
function wib_menu_func( $atts ) {

	$result = <<<EOT
		<nav class="wib-menu">
			<ul>
EOT;
	
	$defaults = array(
		'theme_location'  => '',
		'menu'            => '',
		'container'       => '',
		'container_class' => '',
		'container_id'    => '',
		'menu_class'      => 'menu',
		'menu_id'         => '',
		'before'          => '',
		'after'           => '',
		'link_before'     => '',
		'link_after'      => '',
		'items_wrap'      => '',
		'depth'           => 0,
		'walker'          => ''
	);

	$menu_items = wp_get_nav_menu_items( 'WIB Pages' );
	
	foreach ( (array) $menu_items as $key => $menu_item ) {
	    $title = $menu_item->title;
	    $url = $menu_item->url;
	    $result .= '<li><a href="' . $url . '">' . $title . '</a></li>';
	}

	$result .= <<<EOT
			</ul>
		</nav>
EOT;

	return $result;
	
}
add_shortcode( 'wib_menu', 'wib_menu_func' );

function permalink_thingy($atts) {
	extract(shortcode_atts(array(
		'id' => 1,
		'text' => "",
		'class' => '',
		'style' => ''
    ), $atts));
    
    if ($text) {
        $url = get_permalink($id);
        return "<a href='$url' class='$class' style='$style' >$text</a>";
    } else {
	   return get_permalink($id);
	}
}
add_shortcode('permalink', 'permalink_thingy');
?>