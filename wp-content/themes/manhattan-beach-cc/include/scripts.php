<?php 


add_action('after_setup_theme', 'load_child_scripts', 1);

function load_child_scripts() {
	add_action('wp_enqueue_scripts','register_non_admin_scripts_min');
	add_action( 'wp_enqueue_scripts', 'mbcc_script_assets' );
}



function mbcc_script_assets() {

	// register javascript
	wp_deregister_script('grayscale');
	wp_register_script('grayscale', TEMPLATE_PATH .'/js/grayscale-ck.js');
	wp_enqueue_script('grayscale');	

	// register javascript
	wp_deregister_script('mustache');
	wp_register_script('mustache', TEMPLATE_PATH .'/js/mustache.min.js');
	wp_enqueue_script('mustache');		

	// register javascript
	wp_deregister_script('jqueryui');
	wp_register_script('jqueryui', TEMPLATE_PATH .'/js/jquery.ui.min.js');
	wp_enqueue_script('jqueryui');	

	// register javascript
	wp_deregister_script('app');
	wp_register_script('app', TEMPLATE_PATH .'/js/app.js', array('jquery'), '1.0', true);
	wp_enqueue_script('app');	


	wp_register_script('my_search', TEMPLATE_PATH . "/js/search.js", array('jquery'), '1.0', true);
	wp_enqueue_script('my_search');

	// set up variable(s) for use within script
	wp_localize_script('app', 'WPURLS', array( 'templateDir' => TEMPLATE_PATH )); 
	wp_localize_script('my_search', 'WPURLS', array( 'homeDir' => HOME_URL )); 

}






// Register all scripts
function register_non_admin_scripts_min(){

	global $post;
	global $gdl_is_responsive;
	global $goodlayers_element;		

	// Navigation Menu
	wp_dequeue_script('superfish');	
	wp_register_script('superfish', TEMPLATE_PATH.'/js/parent_scripts/superfish.min.js', false, '1.0', true);
	wp_enqueue_script('superfish');	

	wp_deregister_script('supersub');
	wp_register_script('supersub', TEMPLATE_PATH.'/js/parent_scripts/supersub.min.js', false, '1.0', true);
	wp_enqueue_script('supersub');			
	
	wp_deregister_script('hover-intent');
	wp_register_script('hover-intent', TEMPLATE_PATH.'/js/parent_scripts/hoverIntent.min.js', false, '1.0', true);
	wp_enqueue_script('hover-intent');			
	
	wp_deregister_script('gdl-scripts');
	wp_register_script('gdl-scripts', TEMPLATE_PATH.'/js/parent_scripts/gdl-scripts.min.js', false, '1.0', true);
	wp_enqueue_script('gdl-scripts');
	
	wp_deregister_script('easing');
	wp_register_script('easing', TEMPLATE_PATH.'/js/parent_scripts/jquery.easing.min.js', false, '1.0', true);
	wp_enqueue_script('easing');
	
	wp_deregister_script('prettyPhoto');
	wp_register_script('prettyPhoto', TEMPLATE_PATH.'/js/parent_scripts/jquery.prettyPhoto.min.js', false, '1.0', true);
	wp_enqueue_script('prettyPhoto');	

	$flex_setting = get_gdl_slider_option_array($goodlayers_element['gdl_panel_flex_slider']);
	$flex_setting = array_merge($flex_setting, array('controlsContainer'=>'.flexslider'));	wp_deregister_script('flex-slider');
	wp_register_script('flex-slider', TEMPLATE_PATH.'/js/parent_scripts/jquery.flexslider.min.js', false, '1.0', true);
	wp_localize_script( 'flex-slider', 'FLEX', $flex_setting);
	wp_enqueue_script('flex-slider');	


	// if choosing the responsive option
	if( $gdl_is_responsive ){
		wp_deregister_script('fitvids');
		wp_register_script('fitvids', TEMPLATE_PATH.'/js/parent_scripts/jquery.fitvids.min.js', false, '1.0', false);
		wp_enqueue_script('fitvids');		
	}else{
		wp_deregister_script('preloader');
		wp_register_script('preloader', TEMPLATE_PATH.'/js/parent_scripts/jquery.preloader.min.js', false, '1.0', false);
		wp_localize_script( 'preloader', 'URL', array('goodlayers' => TEMPLATE_PATH) );
		wp_enqueue_script('preloader');
	}
	
	// Search and archive page
	if( is_search() || is_archive() ){

		$flex_setting = get_gdl_slider_option_array($goodlayers_element['gdl_panel_flex_slider']);
		$flex_setting = array_merge($flex_setting, array('controlsContainer'=>'.flexslider'));
	
		wp_deregister_script('flex-slider');
		wp_register_script('flex-slider', TEMPLATE_PATH.'/js/parent_scripts/jquery.flexslider.min.js', false, '1.0', true);
		wp_localize_script( 'flex-slider', 'FLEX', $flex_setting);
		wp_enqueue_script('flex-slider');	
	
	// Post post_type
	}else if( isset($post) &&  $post->post_type == 'post' || 
		isset($post) &&  $post->post_type == 'portfolio'  ){
	
		// If using slider (flex slider)	
		global $gdl_post_thumbnail;
		
		if( $gdl_post_thumbnail == 'Slider'){
		
			$flex_setting = get_gdl_slider_option_array($goodlayers_element['gdl_panel_flex_slider']);
			$flex_setting = array_merge($flex_setting, array('controlsContainer'=>'.slider-wrapper'));
		
			wp_deregister_script('flex-slider');
			wp_register_script('flex-slider', TEMPLATE_PATH.'/js/parent_scripts/jquery.flexslider.min.js', false, '1.0', true);
			wp_localize_script( 'flex-slider', 'FLEX', $flex_setting);
			wp_enqueue_script('flex-slider');	
			
		}
	
	// Page post_type
	}else if( isset($post) &&  $post->post_type == 'page' ){
		
		global $gdl_page_xml, $gdl_top_slider_type, $gdl_top_slider_xml;
		
		//  If using jcarousellite
		if( strpos($gdl_page_xml,'<display-type>Testimonial Category</display-type>') > -1 ){
			wp_deregister_script('jcarousellite');
			wp_register_script('jcarousellite', TEMPLATE_PATH.'/js/parent_scripts/jquery.jcarousellite.min.js', false, '1.0', true);
			wp_enqueue_script('jcarousellite');
		}
		
		// If using nivo slider
		if( strpos($gdl_page_xml,'<slider-type>Nivo Slider</slider-type>') > -1 ||
			$gdl_top_slider_type == 'Nivo Slider' ){
		
			$nivo_setting = get_gdl_slider_option_array($goodlayers_element['gdl_panel_nivo_slider']);
			
			wp_deregister_script('nivo-slider');
			wp_register_script('nivo-slider', TEMPLATE_PATH.'/js/parent_scripts/jquery.nivo.slider.pack.min.js', false, '1.0', true);
			wp_localize_script( 'nivo-slider', 'NIVO', $nivo_setting);
			wp_enqueue_script('nivo-slider');
			
		}
		
		// If using flex slider
		if( strpos($gdl_page_xml, '<slider-type>Flex Slider</slider-type>') > -1 ||
			strpos($gdl_page_xml, '<Portfolio>') > -1 ||
			strpos($gdl_page_xml, '<Blog>') > -1 ||
			$gdl_top_slider_type == 'Flex Slider'){
		
			$flex_setting = get_gdl_slider_option_array($goodlayers_element['gdl_panel_flex_slider']);
			$flex_setting = array_merge($flex_setting, array('controlsContainer'=>'.flexslider'));
		
			wp_deregister_script('flex-slider');
			wp_register_script('flex-slider', TEMPLATE_PATH.'/js/parent_scripts/jquery.flexslider.min.js', false, '1.0', true);
			wp_localize_script( 'flex-slider', 'FLEX', $flex_setting);
			wp_enqueue_script('flex-slider');	
				
		}
		
		// If using anything slider
		if( strpos($gdl_page_xml,'<slider-type>Anything Slider</slider-type>') > -1 ||
			$gdl_top_slider_type == 'Anything Slider' ){
			
			$anything_setting = get_gdl_slider_option_array($goodlayers_element['gdl_panel_anything_slider']);
			
			wp_deregister_script('anythingSlider');
			wp_register_script('anythingSlider', TEMPLATE_PATH.'/js/parent_scripts/jquery.anythingslider.min.js', false, '1.0', true);
			wp_localize_script( 'anythingSlider', 'ANYTHING', $anything_setting);
			wp_enqueue_script('anythingSlider');
			
			// If using video in anything slider
			if( strpos($gdl_page_xml,'<linktype>Link to Video</linktype>') > -1 ||
				strpos($gdl_top_slider_xml,'<linktype>Link to Video</linktype>') > -1 ){
			
				wp_deregister_script('anything-swfobject');
				wp_register_script('anything-swfobject', TEMPLATE_PATH.'/js/parent_scripts/anything-swfobject.min.js', false, '1.0', true);
				wp_enqueue_script('anything-swfobject');	
				
				wp_deregister_script('anythingSlider-video');
				wp_register_script('anythingSlider-video', TEMPLATE_PATH.'/js/parent_scripts/jquery.anythingslider.video.min.js', false, '1.0', true);
				wp_enqueue_script('anythingSlider-video');
								
			}
			
		}
		
		// If using filterable plugin
		if( strpos($gdl_page_xml,'<filterable>Yes</filterable>') > -1 ){
		
			wp_deregister_script('filterable');
			wp_register_script('filterable', TEMPLATE_PATH.'/js/parent_scripts/jquery.filterable.min.js', false, '1.0', true);
			wp_enqueue_script('filterable');				
		
		}
		
		// If use contact-form
		if( strpos($gdl_page_xml,'<Contact-Form>') > -1 ){
		
			wp_deregister_script('contact-form');
			wp_register_script('contact-form', TEMPLATE_PATH.'/js/parent_scripts/gdl-contactform.min.js', false, '1.0', true);
			wp_localize_script( 'contact-form', 'MyAjax', array( 'ajaxurl' => AJAX_URL ) );
			wp_enqueue_script('contact-form');				
					
		}
		
	}

	// Comment Script
	if(is_singular() && comments_open() && get_option('thread_comments')){
	
		wp_enqueue_script( 'comment-reply' ); 
		
	}
	

	
}
?>