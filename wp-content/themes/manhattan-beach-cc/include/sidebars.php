<?php 

register_sidebar( array(
	'name'          => __( 'Footer Top', 'manhattan-beach-cc' ),
	'id'            => 'footer-top',
	'description'   => '',
        'class'         => 'custom-sidebar gdl-divider widget_text',
	'before_widget' => '<li id="%1$s" class="widget %2$s sixteen columns">',
	'after_widget'  => '</li>',
	'before_title'  => '<h3 class="custom-sidebar-title footer-title-color gdl-title">',
	'after_title'   => '</h3>'
)); 

register_sidebar( array(
	'name'          => __( 'Footer Bottom', 'manhattan-beach-cc' ),
	'id'            => 'footer-bottom',
	'description'   => '',
        'class'         => 'custom-sidebar gdl-divider widget_text',
	'before_widget' => '<li id="%1$s" class="widget %2$s four columns">',
	'after_widget'  => '</li>',
	'before_title'  => '<h3 class="custom-sidebar-title footer-title-color gdl-title">',
	'after_title'   => '</h3>'
)); 

?>