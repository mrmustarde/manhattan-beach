<?php

function personnel_metaboxes( $meta_boxes ) {
	$prefix = '_cmb_'; // Prefix for all fields
	$meta_boxes[] = array(
		'id' => 'personnel-details',
		'title' => 'Personnel Details',
		'pages' => array('personnel'), // post type
		'context' => 'normal',
		'priority' => 'high',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
				'name' => 'Staff',
				'desc' => 'check if on staff / empty if on board',
				'id' => $prefix . 'staff',
				'type' => 'checkbox'
			),
			array(
				'name' => 'Bio',
				'desc' => '',
				'id' => $prefix . 'bio',
				'type' => 'wysiwyg'
			)
		),
	);

	return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'personnel_metaboxes' );



// Initialize the metabox class
add_action( 'init', 'be_initialize_cmb_meta_boxes', 9999 );

function be_initialize_cmb_meta_boxes() {
	if ( !class_exists( 'cmb_Meta_Box' ) ) {
		require_once( 'metabox/init.php' );
	}
}

?>