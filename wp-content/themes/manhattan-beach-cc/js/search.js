var monthNames = [ "January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December" ];
var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]




jQuery(function($){

	if(!jQuery("#wp-search-results").length) {
		return;
	}

	var ajaxURL = WPURLS.homeDir + '/api/get_search_results?search=' + searchToObject().q;

	jQuery.ajax({
		url: ajaxURL,
		crossDomain: 'true',
		type:'GET',
		dataType: 'jsonp',
		success:function(results) {
			var posts = results.posts,
				htmlResult = '',
				resultsHeader = jQuery('<h4 class="wpResultsHeader" ></h4>'),
				showResults = jQuery('<a href="#" class="showResults" \>'),
				resultsContainer = jQuery('#wp-search-results');


			for(var key in posts) {
				// if chambermaster page, then continue through loop without this object
				if (posts[key].excerpt == "#CHAMBER_MASTER_CONTENT#") {
					continue;
				}

				var newDate = new Date(),
					htmlDate = '';
				newDate.setDate(newDate.getDate(posts[key].date));



				htmlDate = monthNames[newDate.getMonth()] + ' ' + newDate.getDate() + ' ' + newDate.getFullYear();

				htmlResult += 	'<div class="wp-result">';

				if( posts[key].thumbnail ) {
					htmlResult += '	<div class="mn-image">\
										<a href="' + posts[key].url + '" >\
											<img class="thumbnail" src="' + posts[key].thumbnail + '" \>\
										</a>\
									</div>';
				} else {
					htmlResult += '	<div class="mn-image mn-image-empty"></div>';
				}

				htmlResult += '		<div class="mn-listingcontent">\
										<div class="mn-listing-main" >\
											<div class="mn-title">\
												<a href="' + posts[key].url + '" >' +
													posts[key].title +
												'</a>\
											</div>';
				if( posts[key].type == 'post' ) {
					htmlResult += '			<p class="mn-address">' + htmlDate + '</p>';
				}

				htmlResult += '		</div>\
									<a href="' + posts[key].url + '" class="portfolio-read-more gdl-button">Read More</a>\
						</div>\
							</div>';
			}	
			
			jQuery("#wp-search-results").html(htmlResult);

			if(htmlResult) {

				resultsHeader.html('Pages and Articles').insertBefore(resultsContainer);

				resultsContainer.find('.wp-result:gt(2)').hide();

				// show or hide results
				showResults.html('More pages and articles &raquo;').insertAfter(resultsContainer)
					.click(function(event){
						event.preventDefault();
						resultsContainer.find('.wp-result:gt(0)').fadeIn();
						showResults.hide();
					});
			}
		}
	});

	function searchToObject() {
		var pairs = window.location.search.substring(1).split("&"),
			obj = {},
			pair,
			i;

		for ( i in pairs ) {
		if ( pairs[i] === "" ) continue;

			pair = pairs[i].split("=");
			obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
		}

		return obj;
	}
});