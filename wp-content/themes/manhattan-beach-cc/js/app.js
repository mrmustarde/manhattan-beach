/**
 * jQuery Shuffle (http://mktgdept.com/jquery-shuffle)
 * A jQuery plugin for shuffling a set of elements
 *
 * v0.0.1 - 13 November 2009
 *
 * Copyright (c) 2009 Chad Smith (http://twitter.com/chadsmith)
 * Dual licensed under the MIT and GPL licenses.
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.opensource.org/licenses/gpl-license.php
 *
 * Shuffle elements using: $(selector).shuffle() or $.shuffle(selector)
 *
 **/
(function(d){d.fn.shuffle=function(c){c=[];return this.each(function(){c.push(d(this).clone(true))}).each(function(a,b){d(b).replaceWith(c[a=Math.floor(Math.random()*c.length)]);c.splice(a,1)})};d.shuffle=function(a){return d(a).shuffle()}})(jQuery);


// name and month plugin override
(function() {
    var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    Date.prototype.getMonthName = function() {
        return months[ this.getMonth() ];
    };
    Date.prototype.getDayName = function() {
        return days[ this.getDay() ];
    };
})();




(function($) {





getPatchArticles();

function getPatchArticles() {
	// don't do this if we don't need articles
	if(!$('.patch-articles').length) {
		return;
	}

	// use patch.com rss feed for article list
	parseRSS('http://manhattanbeach.patch.com/articles.rss', showArticles);

	function parseRSS(url, callback) {
		// use ajax json request to get feed via google
		$.ajax({
			url: document.location.protocol + '//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=10&callback=?&q=' + encodeURIComponent(url),
			dataType: 'json',
			success: function(data) {
			  	callback(data.responseData.feed);
			}
		});
	}

	// callback function to show articles from feed
	function showArticles(data) {

		var view = data.entries;

        // only do 5 articles
	    for(var i = 0; i < 5; i++) {
			var date = new Date(view[i].publishedDate);

			var html = '<li >\
	                        <div class="recent-post-widget-title gdl-title">\
	                        	<a href="' + view[i].link + '" target="_blank">' + view[i].title + '</a>\
	                        </div>\
                            <div class="recent-post-widget-date post-info-color">\
								' + date.toDateString() + '\
                            </div>\
	                    </li>';
		    $('.patch-articles').append(html);			    	
	    }
	}
}


$('#mn-memberapp-empfulltime input, #mn-memberapp-empparttime input').change(function(){
	var fullTimeCount = parseInt($('#mn-memberapp-empfulltime input').val()),
		partTimeCount = parseInt($('#mn-memberapp-empparttime input').val()),
		empCount = 0,
		radioSelction = 1;

	if(fullTimeCount && partTimeCount) {
		empCount = fullTimeCount + partTimeCount;
	} else if(fullTimeCount) {
		empCount = fullTimeCount;
	} else if(partTimeCount) {
		empCount = partTimeCount;
	}

	if(empCount) {
		if(empCount <= 2) {
			radioSelction = 1;
		} else if(empCount <=5) {
			radioSelction = 2;
		} else if(empCount <=10) {
			radioSelction = 3;
		} else if(empCount <=15) {
			radioSelction = 4;
		} else if(empCount <=20) {
			radioSelction = 5;
		} else if(empCount <=30) {
			radioSelction = 6;
		} else if(empCount <=50) {
			radioSelction = 9;
		} else if(empCount <=100) {
			radioSelction = 7;
		} else {
			radioSelction = 8;
		}

		$('#mn-memberapp-package .mn-form-radiocombo > label').removeClass('chosen');

		$('#mn-memberapp-memopts input').filter('[value=' + radioSelction + ']').prop('checked', true).parents('label').addClass('chosen');
	}

	
});


// move items around in member details
$('#mn-pagetitle').prependTo($('.mn-member-sidebar'));
$('#mn-member-general .mn-section-content')
	.append($('.mn-memberinfo-block-actions'))
	.append($('#mn-memberinfo-block-socialnetworks'));

$('.mn-memberinfo-block-actions li a').addClass('portfolio-read-more gdl-button');


$('#mn-content #mn-events *').attr('style','');

// personnel page open details
var previewPos = -1,
	counter = 1,
	$prevDetails = 0;

$('.personnel-item').addClass('inactive');

$('.personnel-item').each(
	function(){
		var $item = $(this),
			$personnelContent = $item.find('.personnel-content'),
			$personnelThumbnail = $item.find('.personnel-thumbnail').clone();

		$personnelContent.prepend($personnelThumbnail);
		$personnelContent.wrapInner('<div class="inner" />');
		$('<div class="arrow" />').insertBefore($personnelContent);

		$item.data('offsetTop', $item.offset().top);
	}
);

function resizePersonnelHeader() {
	var titleHeight = 0,
		posHeight = 0,
		$personnelItem = $('.personnel-item');

	$('.personnel-item').each(
		function(){
			var $item = $(this),
				curTitleHeight = $item.find('.personnel-title').height(),
				curPosHeight = $item.find('.personnel-position').height();

			if(curTitleHeight > titleHeight) {
				titleHeight = curTitleHeight;
			}

			if(curPosHeight > posHeight) {
				posHeight = curPosHeight;
			}
		}
	);

	$personnelItem.find('.personnel-title').height(titleHeight);
	$personnelItem.find('.personnel-position').height(posHeight);
}

$(window).resize(resizePersonnelHeader()).trigger('resize');

// click event to show details
$('.personnel-item').click(function(){
	showDetails($(this));
});


function showDetails($container){
	// get offset of item via data
	var height = '300px';

	// if item is clicked again, close it up
	if($container.hasClass('active')) {
		previewPos = -1;  // set up previous offset position

		hideDetails($container);  // hide

		return;
	}

	// hide the old details
	hideDetails($('.personnel-item.active'));
	
	$prevDetails = $container;

	$container.addClass('active');

		$('html, body').animate({
			scrollTop: $('.personnel-item.active').offset().top
		}, 500);
}

function hideDetails($container){
	$container.removeClass('active');
}



//activate the map tab
if($('#mn-maps-container').length) {
	function initMapTab() {
        MNIMemberMap.AutoComplete.Init('#mn-search-keyword :input', { path: '/list/find' });
        MNIMemberMap.Map.Init(362, {
            mapSelector: '#mn-maps-container',
            listSelector: '#mn-maps-list ul',
            resultSelector: '#mn-maps-result',
            submitSelector: '#mn-search-submit :button',
            radiusSelector: '#mn-search-radius :input'
        });
    };
	initMapTab();
}




$().ready(function(){
	var $memberPage = $('#mn-member-results-member'),
		$memberSideBar = $memberPage.find('.mn-member-sidebar'),
		$memberDetails = $memberPage.find('.mn-member-details'),
		$share = $memberPage.find('.mn-actions');

	$memberSideBar.append($share);
});


	


	// make home tourism links the same height
	var maxHeight = 0;
	$('.home .tourism-feature-links').each(function(){
		var curHeight = $(this).height();
		if(curHeight > maxHeight) {
			maxHeight = curHeight;
		}
	}).height(maxHeight);

$().ready(function(){
	// create carousel out of sponsor images in footer
	var $gallery = $('.sponsors .gdl-gallery-item'),
		$newGallery = $('<ul />'),
		content = $gallery.html();

	// rebuild gallery and wrap
	$newGallery.addClass('slides').html(content).insertBefore($gallery).wrap('<div class="flexslider-for-sponsors">');
	$gallery.remove();

	// rebuild gallery to conform to slidshow requirements
	$('.sponsors .gallery-item-wrapper').each(function(){
		var $newGalleryItemWrapper = $('<li />'),
			content = $(this).html();
		
		$newGalleryItemWrapper.html(content).insertBefore($(this));
		$newGalleryItemWrapper.find('.gallery-thumbnail-image').attr('class', 'sponsor-logo');

		$(this).remove();
	});

	// randomize sponsor items
	$('.sponsors li').shuffle();


	// create slideshow with sponsor items
	$('.flexslider-for-sponsors').flexslider({
		animation: "slide",
		minItems: 1,
		maxItems: 4,
		itemWidth: 140,
	    itemMargin: 0,
		controlNav: false,
		slideshow: true,
		animationLoop: true,
		pauseOnHover: true
	});
});



// set up wib menu so the menu items are equal width and take up width of container
var $wibMenu = $('.wib-menu li')

$wibMenu.width( (1 / $wibMenu.length * 100) + "%" );



/* Special links on home page
================================================== */
/*
// change links for bottom right modules in home page
var $title = $('.home h3:contains(Online Directory)').next().find('h2');
$title.appendTo($title.parent()).addClass('bottom-right-module').find('a').html('View Directory').attr('target','_blank');

var $title = $('.home h3:contains(Affordable Care Act Resources)').next().find('h2');
$title.appendTo($title.parent()).addClass('bottom-right-module').find('a').html('Learn More');


// reposition title within content
$('.home .blog-item-holder').each(function(){
	var $this = $(this),
		$container = $this.parent(),
		$title = $container.find('h3'),
		$content = $container.find('.blog-thumbnail-context');

	$content.prepend($title);
	$container.find('div').removeClass('four columns');
});

// desaturate blog images on home page
$('.home .blog-thumbnail-image a').each(function(){
	var $img = $(this).find('img');
	var $gsImg = $img.clone();

	$gsImg.addClass('grayscale');
	$(this).append($gsImg);
	grayscale($gsImg);
});
*/

/* Reorder Chamber Master member items
================================================== */

// loop through member list
$('#mn-members > div').each(
	function() {
		// move image outside of title/address div
		var $imgContainer = $(this).find('.mn-image');

		var url = $imgContainer.prev().find('a').attr('href');
		$imgContainer.insertBefore($imgContainer.parents('.mn-listingcontent'));
		$imgContainer.find('a').attr('href', url).attr('target','_self');

		// add read more button
		var $anchor = $(this).find('.mn-title a'),
			url = $(this).prev().find('a').attr('href');

		$anchor.clone().addClass('portfolio-read-more gdl-button').html('Read More').appendTo($anchor.parents('.mn-listingcontent'));

	}
);

// make registration links in to buttons
$('.mn-actionregister a').addClass('button');


$('#secondDiv').insertBefore('#firstDiv');

/* Reorder Chamber Master member items
================================================== */

// loop through event dates and add date box for each event
$('#mn-events-listings > div').each(function(){
	var $originalDate = $(this).find('.mn-dateday span'),
		rawDate = $originalDate.html(),
		eventDate = new Date(rawDate),
		eventDayName = eventDate.getDayName(),
		eventMonth = eventDate.getMonthName(),
		eventDay = eventDate.getDate(),
		$dateContainer = $('<div />').addClass('dateContainer');
	$dateContainer.html('\
		<span class="eventMonth">' + eventMonth + '</span>\
		<span class="eventDay">' + eventDay + '</span>\
		<span class="eventDayName">' + eventDayName + '</span>\
	');

	$dateContainer.insertBefore($originalDate.parents('.mn-listingcontent'));

	// add read more button
	var $anchor = $(this).find('.mn-title a'),
		url = $(this).prev().find('a').attr('href');

	$anchor.clone().addClass('portfolio-read-more gdl-button').html('Read More').appendTo($anchor.parents('.mn-listingcontent'));

});

// when window has loaded (and inline javascripts) fix dates in home page feeds
$(window).load(function(){
	datesHomeCommEvents();
});

// loop through events on home page and create date box, etc
function datesHomeCommEvents() {
	//$('.home .community-events, .home .chamber-events').parent().removeClass('sixteen columns');

	$('.home .community-events .mn-scroll-item, .home .chamber-events .mn-scroll-item').each(function(){
		//$(this).addClass('four columns wrapper');

		var $originalDate = $(this).find('.mn-scroll-date'),
			rawDate = $originalDate.html(),
			eventDate = new Date(rawDate),
			eventDayName = eventDate.getDayName(),
			eventMonth = eventDate.getMonthName(),
			eventDay = eventDate.getDate(),
			$dateContainer = $('<div />').addClass('dateContainer'),
			$anchor = $(this).find('a'),
			title = $anchor.html(),
			$location = $(this).find('.mn-scroll-location');
		
		$dateContainer.html('\
			<span class="eventMonth">' + eventMonth + '</span>\
			<span class="eventDay">' + eventDay + '</span>\
		');

		$anchor.html('<span class="title">' + title + '</span>').prepend($dateContainer).append($location);
		$originalDate.remove();

	});	

	$('.home .community-events, .home .chamber-events').slideDown();
}


/* Weather on home page 
================================================== */
    var zip = 90266,
    	outer = $('.weather-stunning-text'),
    	inner = $('.weather-stunning-text .stunning-text-content-wrapper'),
    	waveHeight,
    	waterTemp,
    	currTemp,
    	waterData,
    	windSpeed,
    	winddirection,
    	direction;
    	

	getWeather();

	outer.show();

	function getWeather() {
		// don't do this is not at home page
		if(!$('.home').length) {
			return;
		}
		
		// write in weather template
		appendWeatherTemplate(inner);
		
		// get and write new weather data  -- use WPURLS.templateDir from functions.php -> scripts.php
	    $.get(WPURLS.templateDir + '/include/proxy.php?url=http://w1.weather.gov/xml/current_obs/46221.xml', function(data){
			waveHeight = $(data).find('wave_height_ft').text();
			waterTemp = $(data).find('water_temp_f').text();
			waterTemp = Math.floor(waterTemp);
			
	        $('.wave-height .value').html(waveHeight);
	        $('.water-temp .value').html(waterTemp);
		});

	    //var queryStr = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20location%20in%20(%0A%20%20select%20id%20from%20weather.search%20where%20query%3D%22'+ zip +'%22%0A)%20limit%201&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=?';
	    var queryStr = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%3D2444777&format=json&diagnostics=true&callback=';

	    $.getJSON(queryStr,function(data){
	        currTemp = data.query.results.channel.item.condition.temp;
			currTemp = Math.floor(currTemp);
			windSpeed = data.query.results.channel.wind.speed;
			winddirection = data.query.results.channel.wind.direction;

            switch(true)
            {
                case (winddirection==0):
                    direction='N';
                    break;
                case (winddirection<90):
                    direction='NE';
                    break;
                case (winddirection==90):
                    direction='E';
                    break;
                case (winddirection<180):
                    direction='SE';
                    break;
                case (winddirection==180):
                    direction='S';
                    break;
                case (winddirection<270):
                    direction='SW';
                    break;
                case (winddirection==270):
                    direction='W';
                    break;
                case (winddirection<360):
                    direction='NW';
                    break;
                case (winddirection==360):
                    direction='N';
                    break;
            }
	        
	        $('.curr-temp .value').html(currTemp);
	        $('.wind .value').html(windSpeed);
	        $('.wind .direction').html(direction);
        });
	}
		
	function appendWeatherTemplate(container) {
		container.show();
		container.prepend('\
			<div class="weather-container">			\
				<div class="column curr-temp">		\
					<span class="top">				\
						<span class="value"></span>	\
						<span class="degrees">&deg;</span>\
					</span>							\
					<span class="label">Temp</span>	\
				</div>								\
				<div class="column wave-height">	\
					<span class="top">				\
						<span class="value"></span>	\
						<span class="feet">ft</span>\
					</span>							\
					<span class="label">Swell</span>\
				</div>								\
				<div class="column water-temp">		\
					<span class="top">				\
						<span class="value"></span>	\
						<span class="degrees">&deg;</span>\
					</span>							\
					<span class="label">Water</span>\
				</div>								\
				<div class="column wind">		\
					<span class="top">				\
						<span class="value"></span>	\
						<span class="direction feet"></span>\
					</span>							\
					<span class="label">Wind</span>\
				</div>								\
			</div>									\
		');
		

	}







	registerROICalc();

	function registerROICalc() {
		buildROI();

		$('#numEmployees').focus();


		// on change, calculate ROI for this page
		$('#numEmployees, #avgSales, #sales').on('keyup', function(){
			roiErrors($(this));
			calculateROI();
		});

		function buildROI() {
			var initialValue = 5;

			var sliderTooltip = function(event, ui) {
			    var curValue = ui.value || initialValue;
			    if (curValue == 200) {
			    	curValue += "+";
			    }
			    var tooltip = '<div class="tooltip"><div class="tooltip-inner">' + curValue + '</div><div class="tooltip-arrow"></div></div>';

			    $('.calculator .ui-slider-handle').html(tooltip);

			    calculateROI();
			}

			$(".calculator #slider").slider({
			    value: initialValue,
			    min: 1,
			    max: 200,
			    create: sliderTooltip,
			    slide: sliderTooltip
			});	
		}

		function roiErrors($element) {
			$('.error-message').remove();
			$('.error').removeClass('error');
			
			if( isNaN( $element.val() ) ) {
				$element.select().addClass('error').after('<span class="error-message">Error: Use a number</span>');
			}
		}

		function calculateROI() {
			var numEmployees, avgSales, officeDepotSavings, onlineDir, printDir, socialMedia, sandDollar, networking, merchServices, cityCouncilAdvocacy, beachReporter, totalCosts, totalSavings, totalROI;

//			numEmployees = parseInt($('#numEmployees').val());
			numEmployees = parseInt($('#slider .tooltip-inner').html());
			avgSales = parseInt($('#sales').val());

			// leave function if NaN
			if(isNaN(numEmployees) || isNaN(avgSales)) {
				return;
			}

			officeDepotSavings = numEmployees * 110;
			merchServices = avgSales * 0.01;
			beachReporter = 250;

			totalSavings = 	officeDepotSavings +
							merchServices + 
							beachReporter;


			onlineDir = avgSales * 0.0015;
			printDir = avgSales * 0.000375;
			socialMedia = avgSales * 0.00175;
			sandDollar = avgSales * 0.00125;
			networking = avgSales * 0.003;
			cityCouncilAdvocacy = 1500;

			totalCosts = 	onlineDir +
							printDir +
							socialMedia +
							sandDollar;
							//networking +
							//cityCouncilAdvocacy;

			totalROI = totalSavings - totalCosts;

			var roiClass = 'zero';
			if(totalROI > 0) {	
				roiClass = 'positive';
			} else if (totalROI < 0 ) {
				roiClass = 'negative';
			}

			var result = {
				savings : [
					{
						name : "Office Depot Savings",
						value : officeDepotSavings.toFixed()
					},

					{
						name : "Merchant Services Savings",
						value : merchServices.toFixed()
					},													
					{
						name : "Beach Reporter/Daily Breeze Savings",
						value : beachReporter.toFixed()
					}
				],

				costs : [
					{
						name : "Online Directory",
						value : onlineDir.toFixed()
					},							
					{
						name : "Print Directory",
						value : printDir.toFixed()
					},							
					{
						name : "Social Media",
						value : socialMedia.toFixed()
					},							
					{
						name : "Sand Dollar",
						value : sandDollar.toFixed()
					},							
					{
						name : "Networking",
						value : networking.toFixed()
					},
					{
						name : "City Council Advocacy",
						value : cityCouncilAdvocacy.toFixed()

					},						
				],

				totalCosts : numberWithCommas(totalCosts.toFixed()),
				totalSavings : numberWithCommas(totalSavings.toFixed()),
				totalROI : numberWithCommas(totalROI.toFixed()),
				roiClass : roiClass
			}

			$('.totalROI').html("$" + result.totalROI);

			var template = '',
				html = '';


			template = "<tr class=benefits'><td class='label'>Your Chamber Benefits</td><td class='amount'>${{totalSavings}}</td></tr>";

			html += Mustache.to_html(template, result);

			template = "<tr class=investment'><td class='label'>Your Chamber Investment</td><td class='amount'>${{totalCosts}}</td></tr>";

			html += Mustache.to_html(template, result);

			template = "{{#savings}}<tr class='savings'><td>{{name}}</td><td>${{value}}</td></tr>{{/savings}}";

			html += Mustache.to_html(template, result);

			template = "{{#costs}}<tr class='costs'><td>{{name}}</td><td>${{value}}</td></tr>{{/costs}}";

			html += Mustache.to_html(template, result);

			$('.tally tbody').html(html);
		}
	}
     
})(jQuery);

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}