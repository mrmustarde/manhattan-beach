<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mbc_dev_');

/** MySQL database username */
define('DB_USER', 'mbc_dev_');

/** MySQL database password */
define('DB_PASSWORD', 'mbch4mb3r');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HCF4j)>VQ{-SEJH4KhooGL}u9l72H<!U}%GUWVXQ|W &oF+4<3WX|n.AbLiN4mEx');
define('SECURE_AUTH_KEY',  '|G|z{~i|CTjk+JFM- VjQ5Gsgf~r/PNP{-mZi`acx y:J_-4%J?DC9+d`$*QBrW{');
define('LOGGED_IN_KEY',    'nuX:7N|$dzSTYf^;#TSb*Q1gS+3VatnstUE%Xf#R8Q:#HiBdzl+5f:)JEF!{H41?');
define('NONCE_KEY',        '^Umw,xev#>e-.biq4!~|e-#SkcyviVfb>,!iu(---Fh+y`+;>`x>]c5+1O>)t#`_');
define('AUTH_SALT',        'o`v}pBGH#7,!EA0fzzf5S-r{[+msA<m=ND]tAN+b$-^s&2#yi5xrBmx^*S?U271G');
define('SECURE_AUTH_SALT', '`%>. dQCSkxMi^ZX@@;gMrXubPe:eHeO5x3z`,k@fD=C;MfT D>V9chi+hz>s3E[');
define('LOGGED_IN_SALT',   'XeMJ/-*6fiT$h-p+f;d{@j^gmr<([&VAsHOdlIc5f[AcYQ!_]UP=:ulR7=Az|H-|');
define('NONCE_SALT',       'Z7oE8GlzJ5|(@AgKu|+U# G(F~DhQ4|ur{a8X;E$b;6c,8U3x~C1KFibds.yy>4$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

?>